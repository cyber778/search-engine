from django.contrib import admin
from search.models import Index, Page, Word

admin.site.register(Index)
admin.site.register(Page)
admin.site.register(Word)