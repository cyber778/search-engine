# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models



class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Page'
        db.create_table('search_page', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('page_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('rank', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('search', ['Page'])

        # Adding model 'Word'
        db.create_table('search_word', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('search', ['Word'])

        # Adding M2M table for field page_list on 'Word'
        m2m_table_name = db.shorten_name('search_word_page_list')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('word', models.ForeignKey(orm['search.word'], null=False)),
            ('page', models.ForeignKey(orm['search.page'], null=False))
        ))
        db.create_unique(m2m_table_name, ['word_id', 'page_id'])

        # Adding model 'Index'
        db.create_table('search_index', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('rebuild', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('search', ['Index'])

        # Adding M2M table for field word_list on 'Index'
        m2m_table_name = db.shorten_name('search_index_word_list')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('index', models.ForeignKey(orm['search.index'], null=False)),
            ('word', models.ForeignKey(orm['search.word'], null=False))
        ))
        db.create_unique(m2m_table_name, ['index_id', 'word_id'])


    def backwards(self, orm):
        # Deleting model 'Page'
        db.delete_table('search_page')

        # Deleting model 'Word'
        db.delete_table('search_word')

        # Removing M2M table for field page_list on 'Word'
        db.delete_table(db.shorten_name('search_word_page_list'))

        # Deleting model 'Index'
        db.delete_table('search_index')

        # Removing M2M table for field word_list on 'Index'
        db.delete_table(db.shorten_name('search_index_word_list'))


    models = {
        'search.index': {
            'Meta': {'object_name': 'Index'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'rebuild': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'word_list': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['search.Word']", 'symmetrical': 'False'})
        },
        'search.page': {
            'Meta': {'object_name': 'Page'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rank': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'search.word': {
            'Meta': {'object_name': 'Word'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'page_list': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['search.Page']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['search']