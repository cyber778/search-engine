# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Page.page_name'
        db.delete_column('search_page', 'page_name')

        # Adding field 'Page.name'
        db.add_column('search_page', 'name',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=100),
                      keep_default=False)


        # Changing field 'Page.description'
        db.alter_column('search_page', 'description', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Page.created'
        db.alter_column('search_page', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, null=True))

        # Changing field 'Page.title'
        db.alter_column('search_page', 'title', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Page.content'
        db.alter_column('search_page', 'content', self.gf('django.db.models.fields.TextField')(null=True))

    def backwards(self, orm):
        # Adding field 'Page.page_name'
        db.add_column('search_page', 'page_name',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=100),
                      keep_default=False)

        # Deleting field 'Page.name'
        db.delete_column('search_page', 'name')


        # Changing field 'Page.description'
        db.alter_column('search_page', 'description', self.gf('django.db.models.fields.TextField')(default=2))

        # Changing field 'Page.created'
        db.alter_column('search_page', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=2))

        # Changing field 'Page.title'
        db.alter_column('search_page', 'title', self.gf('django.db.models.fields.CharField')(default=2, max_length=100))

        # Changing field 'Page.content'
        db.alter_column('search_page', 'content', self.gf('django.db.models.fields.TextField')(default=2))

    models = {
        'search.index': {
            'Meta': {'object_name': 'Index'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'rebuild': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'word_list': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['search.Word']", 'null': 'True', 'blank': 'True'})
        },
        'search.page': {
            'Meta': {'object_name': 'Page'},
            'content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rank': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'search.word': {
            'Meta': {'object_name': 'Word'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'page_list': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['search.Page']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['search']