from django.db import models


class Page(models.Model):
    nam = models.CharField(max_length=100)
    title = models.CharField(max_length = 100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now=True, null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    rank = models.IntegerField(default=0)
    href = models.CharField(max_length=1000, default='')
    def __unicode__(self):
        return self.nam

class Word(models.Model):
    name = models.CharField(max_length=100)
    page_list = models.ManyToManyField(Page, null=True, blank=True)
    def __unicode__(self):
        return self.name
    
class Index(models.Model):
    name = models.CharField(max_length=40)
    word_list = models.ManyToManyField(Word, null=True, blank=True)
    rebuild = models.BooleanField(default=False)
    def __unicode__(self):
        return self.name