from django.shortcuts import render
from search.models import Page, Word, Index
import urllib2
from bs4 import BeautifulSoup
import operator


REVIEW_LIST = ["a","and","is","there","the","to","for","in","be","an","of"]
#!!!!dont forget to create main and some pages
# Create your views here.
def file_import(request):
    if request.method == 'POST':
        url = request.POST['url']
        if(Page.objects.filter(href=url).count()>0):
            return render(request, 'home3.html', {
            'title': False
        })
        url = request.POST['url']
        try:
            page = urllib2.urlopen(url)
        except:
            return render(request, 'home3.html', {
            'error': True
        })
        soup = BeautifulSoup(page)
        for elem in soup.findAll(['script', 'style']):
            elem.extract()
        content = soup.body.get_text(separator=u' ')
        title = soup.title.get_text(separator=u' ')
        description = content[:150]
        page = Page(nam=title,title=title,description=description,content=content,href=url)
        page.save()
        index = Index.objects.get(name='main')
        index.rebuild = True
        index.save()
        return render(request, 'home3.html', {
            'url': url,
            'title': title,
            'content': content,
        })
    else:
        return -1
    return

def index_update():
    index = Index.objects.get_or_create(name='main')
    index = index[0]
    if(not index.rebuild):
        return
    index.rebuild = False
    index.save()
# only update because main already created
    pages = Page.objects.all()
    checked_for_index = []
    for p in pages:
        checked_for_page = []
        page_words = p.content.split()
        for w in page_words:
            if(w in REVIEW_LIST):
                continue
            if(w in checked_for_page):
                continue
            checked_for_page.append(w)
            word = Word.objects.get_or_create(name=w)
            word = word[0]
            word.page_list.add(p)
            word.save()
            if(w in checked_for_index):
                continue
            index.word_list.add(word)
            index.rebuild = False
            index.save()
    return

def rank_reset():
    pages = Page.objects.all()
    for p in pages:
        p.rank = 0
    return
            
def search(request):
    if request.method == 'POST':
        index_update()
        rank_reset()
        string = request.POST['string']
        string2 = request.POST['string2']
        and_or = request.POST['and_or']
        if(and_or == '1'):
            string+= " " + string2
        lookup_type = request.POST['lookup_type']
        lookup_type2 = request.POST['lookup_type2']
        words = string.split()
        if(and_or == '2' or and_or == '3'):
            words2 = string2.split()
        pages =[]
        #get the first input pages
        for w in words:
            if(lookup_type == '0'): # 0exact 1iexact 2icontains 3contains
                if(not Word.objects.filter(name=w).count()>0):
                    continue
                word = Word.objects.get(name=w)
                pages = word.page_list.all()
            elif (lookup_type == '1'):
                if(not Word.objects.filter(name__iexact=w).count()>0):
                    continue
                word = Word.objects.filter(name__iexact=w)
                for i in word:
                    pages+=i.page_list.all()
            elif (lookup_type == '2'):
                if(not Word.objects.filter(name__contains=w).count()>0):
                    continue
                word = Word.objects.filter(name__contains=w)
                for i in word:
                    pages+=i.page_list.all()
            else:
                if(not Word.objects.filter(name__icontains=w).count()>0):
                    continue
                word = Word.objects.filter(name__icontains=w)
                for i in word:
                    pages+=i.page_list.all()
            if(pages == []):
                continue
            if (lookup_type == '1' or lookup_type == '3' ):
                for p in pages:
                    p.rank += p.content.count(w.lower())
            else:   
                for p in pages:
                    p.rank += p.content.count(w)
                    
                    
        if and_or =='2' or and_or =='3':
            
            pages2 = []
            for w2 in words2:
                if(lookup_type2 == '0'): # 0exact 1iexact 2icontains 3contains
                    if(not Word.objects.filter(name=w2).count()>0):
                        continue
                    word2 = Word.objects.get(name=w2)
                    pages2 = word2.page_list.all()
                elif (lookup_type2 == '1'):
                    if(not Word.objects.filter(name__iexact=w2).count()>0):
                        continue
                    word2 = Word.objects.filter(name__iexact=w2)
                    for i in word2:
                        pages2+=i.page_list.all()
                elif (lookup_type2 == '2'):
                    if(not Word.objects.filter(name__contains=w2).count()>0):
                        continue
                    word2 = Word.objects.filter(name__contains=w2)
                    for i in word2:
                        pages2+=i.page_list.all()
                else:
                    if(not Word.objects.filter(name__icontains=w2).count()>0):
                        continue
                    word2 = Word.objects.filter(name__icontains=w2)
                    for i in word2:
                        pages2+=i.page_list.all()
                if(pages2 == []):
                    continue
                if (lookup_type2 == '1' or lookup_type == '3' ):
                    for p in pages2:
                        p.rank += p.content.count(w2.lower())
                else:   
                    for p in pages2:
                        p.rank += p.content.count(w2)
            
            pages3=[]    
            if and_or =='2':
                for p2 in pages2:
                    if p2 in pages:
                        pages3.append(p2)
                       
               
            elif and_or =='3':     
                for p in pages:
                    if p not in pages2:
                        pages3.append(p)
            pages = pages3
            
            
        pages=list(set(pages))
        if(pages == []):
            return render(request, 'home2.html', {
            'string': string,
            'pages': False,
        })

        pages.sort(key=operator.attrgetter('rank'))
        pages = reversed(pages)
        return render(request, 'home2.html', {
            'string': string,
            'pages': pages,
        })
    else:
        return render(request, 'home.html', {
            'error_message': "You didn't select a choice.",
        })